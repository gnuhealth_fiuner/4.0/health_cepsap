# This file is part of health_cepsap module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

try:
    from trytond.modules.health_cepsap.tests.tests import suite
except ImportError:
    from .tests import suite

__all__ = ['suite']
