
Módulo GNU Health para la Clasificación Estadística de Problemas de Salud
en Atención Primaria (CEPS-AP)

Prerequisites
-------------

 * Python 3.4 or later (http://www.python.org/)
 * Tryton 4.2 (http://www.tryton.org/)

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

  Silix
  --------------
  website: http://www.silix.com.ar
  email: contacto@silix.com.ar

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
